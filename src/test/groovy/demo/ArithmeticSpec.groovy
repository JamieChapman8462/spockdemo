package demo

import org.junit.Assert
import spock.lang.Specification
import spock.lang.Unroll


class ArithmeticSpec extends Specification {

    @Unroll
    def "test add value a: #a and value b: #b"() {

        when:
        true
        then:
        true
    }

    def "test addWithExternalNumber"() {
        given:
        Numbers numbers = Mock(Numbers)
        Arithmetic arithmetic =  new Arithmetic(numbers: numbers)
        numbers.getNumber() >> 6
        when:
        "a" == "b"
        then:
        def a = "fdfd"
        true
    }
//
//    def "test subtract"() {
//        given:
//
//        when:
//        // TODO implement stimulus
//        then:
//        // TODO implement assertions
//    }
//
    def "test divideByZero"() {
        given:
        Arithmetic arithmetic = new Arithmetic()
        when:
        arithmetic.divideByZero()
        then:
        noExceptionThrown()
        where:
        value << ["a", "dsds", new String("dsdsd")]
    }
}
