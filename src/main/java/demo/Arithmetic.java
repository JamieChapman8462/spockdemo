package demo;

public class Arithmetic {

    private Numbers numbers;

    public int add(int a, int b) {
        return a + b;
    }

    public int addWithExternalNumber(int a) {
        return a + numbers.getNumber();
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int  divideByZero(){return 0 / 0;}
}
